{ lib, fetchFromGitHub, pkgs, stdenv }:
stdenv.mkDerivation rec {
  pname = "cork";
  version = "unstable";

  src = fetchFromGitHub {
    owner = "gilbo";
    repo = "cork";
    rev = "5987de50801d1ce3dedc91307d478594459662d6";
    sha256 = "sha256-CmSq3vFddgFmSwjtGOI5s5imZMvsjNgPi43BdQxCNwI=";
  };

  nativeBuildInputs = [ ];
  buildInputs = [
    pkgs.clang
    pkgs.llvm
    pkgs.gmp
  ];

  installPhase = ''
    mkdir -p $out/bin
    mkdir -p $out/lib64
    mkdir -p $out/include

    cp bin/* $out/bin/
    cp lib/* $out/lib64/
    cp include/* $out/include/
  '';

  meta = with lib; {
    description = "Cork - a powerful standalone boolean calculations software";
    longDescription = ''
      Cork is designed to support Boolean operations between triangle meshes.
    '';
    homepage = "https://github.com/gilbo/cork";
    license = licenses.gpl3;
    maintainers = [ "brothermechanic\u00e8s <brothermechanic@gmail.com>" ];
    platforms = platforms.all;
    priority = 6; # resolves collision
  };
}
