{ system ? builtins.currentSystem, pkgs ? import <nixpkgs> { inherit system; } }:

rec {
  cork = pkgs.callPackage ./pkgs/cork { };
}
